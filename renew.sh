#!/usr/bin/env bash

log="/var/log/renew.log"

echo "[$(date)] -- Renewing certbot" &>> $log
certbot renew --post-hook "nginx -s reload" &>> $log
