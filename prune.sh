#!/usr/bin/env bash

echo "[$(date)] -- Pruning Docker" &>> /var/log/prune.log
docker system prune -f &>> /var/log/prune.log
